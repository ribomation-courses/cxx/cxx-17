#include <iostream>
#include <string>
#include <stdexcept>
#include <cstring>
#include <csignal>
#include "trace.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

namespace ribomation::signals {
    struct AsyncException : runtime_error {
        AsyncException(int signo)
                : runtime_error{strsignal(signo)
                                + " ("s + to_string(signo) + ")"s} {}
    };

    void crashHandler(int signo) {
        auto t = Trace{"crash-handler: signo=" + to_string(signo)};
        throw AsyncException{signo};
    }

    void registerSignal(int signo, sighandler_t handler) {
        struct sigaction s{};
        s.sa_handler = handler;
        s.sa_flags   = SA_NODEFER;
        if (sigaction(signo, &s, nullptr) != 0)
            throw invalid_argument{"Failed to register signal handler"};

        sigset_t signals{};
        sigaddset(&signals, signo);
        if (sigprocmask(SIG_UNBLOCK, &signals, nullptr) != 0)
            throw invalid_argument{"Failed to setup signal"};
    }
}


void bizop(int idx) {
    auto t = Trace{"bizop"};
    t.print("*boom*");
    int* ptr = nullptr;
    *ptr = idx;
}

void func2(int idx) {
    auto t = Trace{"func2()"};
    bizop(idx);
}

void func1(int idx) {
    auto t = Trace{"func1()"};
    func2(idx);
}

int main() {
    auto m = Trace{"main()"};
    using ribomation::signals::registerSignal;
    using ribomation::signals::crashHandler;
    registerSignal(SIGSEGV, &crashHandler);

    auto const N = 3U;
    for (auto k = 1U; k <= N; ++k) {
        try {
            auto t = Trace{"try-" + to_string(k)};
            func1(42);
        } catch (const exception& x) {
            cout << "ERROR: " << x.what() << endl;
        }
    }
    return 0;
}


