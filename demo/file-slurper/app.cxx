#include <iostream>
#include "file-slurper.hxx"
using namespace std;
using namespace ribomation;

int main() {
    for (auto line : FileSlurper{"../app.cxx"}) {
        cout << "** " << line << endl;
    }
    return 0;
}
