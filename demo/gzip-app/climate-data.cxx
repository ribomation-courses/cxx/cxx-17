#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <string_view>
#include <filesystem>
#include <unordered_map>
#include <map>
#include <chrono>
#include <charconv>

#include <cerrno>
#include <cstring>

#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filter/gzip.hpp>

using namespace std;
using namespace std::literals;
namespace fs = std::filesystem;
namespace bio = boost::iostreams;

struct Observation {
    unsigned year    = 0;
    double   celsius = 0;
    unsigned count   = 0;

    Observation() = default;

    Observation(string_view date, string_view temp) {
        from_chars(date.begin(), date.begin() + 4, year);
        float fahrenheit = stof(string(temp));
        celsius = (fahrenheit - 32.0) * 5.0 / 9.0;
    }

    void update(const Observation& obs) {
        celsius += obs.celsius;
        ++count;
    }

    void compute() {
        celsius = celsius / count;
    }

    friend auto operator <<(ostream& os, const Observation& obs) -> ostream& {
        return os << obs.year << ": " << obs.celsius << "C";
    }
};


int main(int argc, char** argv) {
    // Download the large file from:
    // https://docs.ribomation.se/java/java-8/climate-data.txt.gz
    // Then, update the folder path below

    auto dir          = fs::path("../../../../../../../large-files"s);
    if (!fs::is_directory(dir)) {
        throw invalid_argument{"cannot find folder "s + dir.string()};
    }

    auto filename = dir / "climate-data.txt.gz"s;

    auto file = ifstream{filename, ios_base::in | ios_base::binary};
    if (!file) throw invalid_argument{"cannot open "s + filename.string() + ": "s + strerror(errno)};

    auto zin = bio::filtering_streambuf<bio::input>{};
    zin.push(bio::gzip_decompressor());
    zin.push(file);
    auto in = istream{&zin};

    cout << "Processing file " << filename << " ...\n";
    auto        startTime   = chrono::steady_clock::now();
    auto        climateData = unordered_map<unsigned, Observation>{};
    const auto  heading     = "STN---"sv;
    for (string line; getline(in, line);) {
        auto buf = string_view{line.data(), line.size()};
        if (heading == buf.substr(0, 6)) continue;

        //319150 99999  19930125    11.4  7    10.7  ...
        //0123456789012345678901234567890123456789
        //..........1.........2.........3
        auto date = buf.substr(14, 8);
        auto temp = buf.substr(25, 5);
        auto obs  = Observation{date, temp};
        climateData[obs.year].update(obs);
    }

    auto result = map<unsigned, Observation>{climateData.begin(), climateData.end()};
    for (auto&[year, obs] : result) {
        obs.compute();
        cout << year << ": " << setprecision(3) << obs.celsius << "C\n";
    }

    auto endTime = chrono::steady_clock::now();
    cout << "elapsed: " << chrono::duration_cast<chrono::seconds>(endTime - startTime).count() << " secs" << endl;

    return 0;
}
