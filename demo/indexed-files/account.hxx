#pragma once

#include "db/text.hxx"
#include "db/real.hxx"
#include "db/integer.hxx"
#include "db/boolean.hxx"

namespace bank {
    using namespace ribomation::io;

    class Account {
        Text<16>    accno;
        Real<12, 2> balance;
        Integer<9>  count;
        Boolean     negative;
        Text<4>     filler;

    public:
        Account(const string& accno, float balance)
                : accno{accno}, balance{balance}, count{1}, negative{balance < 0} {}

        Account() = default;
        ~Account() = default;
        Account(const Account&) = default;
        Account& operator =(const Account&) = default;

        float getBalance() const {
            return balance.value();
        }
        
        void update(float amount) {
            balance += amount;
            ++count;
            negative = (balance < 0);
        }
        
        Account& operator +=(float amount) {
            update(amount);
            return *this;
        }

        friend ostream& operator <<(ostream& os, const Account& acc) {
            return os << "Account{" << acc.accno
                      << ", SEK " << acc.balance
                      << ", count=" << acc.count
                      << ", negative=" << (acc.negative ? "Yes" : "No")
                      << "}";
        }
    };

}

