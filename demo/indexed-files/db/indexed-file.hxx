#pragma once

#include <fstream>
#include <stdexcept>
#include <unordered_map>

namespace ribomation::io {
    using namespace std;

    extern "C" { // System API functions
    int truncate(const char* path, size_t length);
    }

    enum class OpenMode {
        read, write, readwrite
    };

    template<typename Record>
    class IndexedFile {
        constexpr static size_t       SIZE = sizeof(Record);
        const string                  filename;
        const OpenMode                openMode;
        fstream                       file;
        unordered_map<string, size_t> index;

        static ios_base::openmode asIosMode(const OpenMode& mode) {
            ios_base::openmode m = ios_base::binary;
            switch (mode) {
                case OpenMode::read:
                    m |= ios_base::in;
                    break;
                case OpenMode::write:
                    m |= ios_base::out;
                    break;
                case OpenMode::readwrite:
                    m |= ios_base::in | ios_base::out;
                    break;
                default:
                    throw invalid_argument("Invalid open mode");
            }
            return m;
        }

        void buildIndex() {
            index.clear();
            size_t    k = 0;
            for (auto r : (*this)) {
                index[r.indexKey()] = k++;
            }
        }

        void move(size_t to, size_t from) {
            auto x = file.tellg();
            file.seekg(from * SIZE);
            char buf[SIZE];
            file.read(reinterpret_cast<char*>(buf), SIZE);
            file.seekp(to * SIZE);
            file.write(reinterpret_cast<const char*>(buf), SIZE);
        }

        void shift(size_t pos) {
            const auto N = count() - 1;
            for (auto  k = pos; k < N; ++k) {
                move(k, k + 1);
            }
        }

        static void read(fstream& db, size_t pos, Record& r) {
            db.seekg(pos * SIZE);
            db.read(reinterpret_cast<char*>(&r), sizeof(Record));
        }

        static void write(fstream& db, size_t pos, const Record& r) {
            db.seekp(pos * SIZE);
            db.write(reinterpret_cast<const char*>(&r), sizeof(Record));
        }

        void append(fstream& db, const Record& r) {
            db.seekp(0, ios_base::end);
            db.write(reinterpret_cast<const char*>(&r), sizeof(Record));
        }

    public:
        IndexedFile(const string& filename, OpenMode mode) : filename{filename}, openMode{mode} {
            file.open(filename, asIosMode(openMode));
            if (!file) throw invalid_argument("cannot open " + filename);
        }

        ~IndexedFile() = default;
        IndexedFile() = delete;
        IndexedFile(const IndexedFile<Record>&) = delete;
        IndexedFile<Record>& operator =(const IndexedFile<Record>&) = delete;

        unsigned long size() {
            file.seekg(0, ios_base::end);
            return static_cast<size_t>(file.tellg());
        }

        unsigned count() {
            return static_cast<unsigned>(this->size() / SIZE);
        }

        IndexedFile<Record>& operator <<(const Record& r) {
            if (openMode == OpenMode::read)
                throw invalid_argument("file not open for writing");
            append(file, r);
            return *this;
        }

        struct iterator {
            fstream& db;
            size_t pos;

            iterator(fstream& db, size_t pos) : db{db}, pos{pos} {}

            bool operator !=(const iterator& that) const {
                return this->pos < that.pos;
            }

            void operator ++() { this->pos++; }

            Record operator *() {
                Record r;
                read(db, pos, r);
                return r;
            }

            operator Record() {
                return operator *();
            }

            void operator =(const Record& r) {
                write(db, pos, r);
            }
        };

        iterator begin()  { return {file, 0}; }
        iterator end()  { return {file, count()}; }

        iterator operator [](size_t pos) {
            if (pos > count()) {
                throw out_of_range("index too large - current size is " + to_string(count()));
            }
            return {file, pos};
        }

        iterator operator [](const string& idx) {
            if (index.empty() && count() > 0) {
                buildIndex();
            }
            if (index.count(idx) == 0) {
                throw out_of_range("no index for " + idx + " found");
            }
            return (*this)[index[idx]];
        }

        void erase(size_t pos) {
            const auto N = count();
            if (N == 0) {
                throw out_of_range("can not erase, database empty");
            }
            if (pos > N) {
                throw out_of_range("index too large - current size is " + to_string(count()));
            }
            shift(pos);
            truncate(filename.c_str(), (N - 1) * SIZE);
            index.clear();
        }

        void erase(const string& idx) {
            if (index.empty() && count() > 0) {
                throw out_of_range("can not erase, database empty");
            }
            if (index.count(idx) == 0) {
                throw out_of_range("no index for " + idx + " found");
            }
            erase(index[idx]);
        }

    };

}

