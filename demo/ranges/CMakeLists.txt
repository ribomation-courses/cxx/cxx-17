cmake_minimum_required(VERSION 3.10)
project(ranges)

include_directories(/home/jens/Tools/range-v3/include)

set(CMAKE_CXX_STANDARD 14)
add_compile_options(-Wall  -Wfatal-errors)

add_executable(simple-pipeline simple-pipeline.cxx)

