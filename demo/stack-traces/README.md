# Stack Traces in C++

This is a small _proof-of-concept_ library demonstrating how to obtain
stack traces when an exception is thrown. In addition, it demonstrates
how to catch and obtain a stack trace from a crash such as
* Null-pointer (SIGSEGV)
* Division by zero (SIGFPE)

## Pre-Requisites
This library uses GCC specific ABI calls.
Ensure you compile the project using GCC/G++, supporting C++17.
In addition, you need to have the build tools installed
* Make (or Ninja)
* CMake

## Build
Create a build dir and run cmake

    mkdir bld && cd bld
    cmake ..
    cmake --build .

## Run the Unit Tests
Inside the `bld/` folder run

    ./unit-test

## How to Use
View the various unit test files in the `test/` folder.

