#include <iostream>
#include <sstream>
#include <initializer_list>
#include <algorithm>
#include <numeric>

using namespace std;

class SimpleStats {
    unsigned _count;
    float    sum, _max, _min;
public:
    SimpleStats(initializer_list<float> args) {
        _count = static_cast<unsigned>(args.size());
        _min   = *min_element(args.begin(), args.end());
        _max   = *max_element(args.begin(), args.end());
//        auto [mn, mx] = minmax_element(args.begin(), args.end());
//        cout << mn << mx;
        sum = accumulate(args.begin(), args.end(), 0);
    }

    unsigned count() const { return _count; }
    float min() const { return _min; }
    float max() const { return _max; }
    float mean() const { return sum / _count; }

    string toString() const {
        ostringstream buf;
        buf << "Stats{avg=" << mean()
            << ", min/max=" << min() << "/" << max()
            << ", cnt=" << count() << "}";
        return buf.str();
    }

    friend ostream& operator <<(ostream& os, const SimpleStats& s) {
        return os << s.toString();
    }

    ~SimpleStats() = default;
    SimpleStats(const SimpleStats&) = default;
    SimpleStats& operator =(const SimpleStats&) = default;
};


int main() {
    {
        SimpleStats s = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        cout << "#1) " << endl;
        cout << "count  : " << s.count() << endl;
        cout << "average: " << s.mean() << endl;
        cout << "min/max: " << s.min() << " / " << s.max() << endl;
    }
    {
        cout << "#2) " << SimpleStats{10, 20, 30, 40, 50} << endl;
    }
    return 0;
}
