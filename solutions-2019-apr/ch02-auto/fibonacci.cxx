#include <iostream>
#include <string>
#include <tuple>
#include <map>
#include <utility>

using namespace std;

auto fibonacci(unsigned n) {
    if (n == 0)return 0U;
    if (n == 1)return 1U;
    return fibonacci(n - 2) + fibonacci(n - 1);
}

auto compute(unsigned n) {
    return make_tuple(n, fibonacci(n));
}

auto populate(unsigned n) {
    auto      results = map<unsigned, unsigned>{};
    for (auto k       = 1U; k <= n; ++k) {
        auto[arg, fib] = compute(k);
        results[arg] = fib;
    }
    return results;
}

int main(int argc, char** argv) {
    auto N = (argc <= 1) ? 10U : stoi(argv[1]);

    cout << "--- #1 ---\n";
    for (auto[arg, fib] : populate(N))
        cout << "fib(" << arg << ") = " << fib << endl;

    cout << "--- #2 ---\n";
    for (auto const& e : populate(N))
        cout << "fib(" << e.first << ") = " << e.second << endl;

    cout << "--- #3 ---\n";
    for (pair<const unsigned, const unsigned> e : populate(N))
        cout << "fib(" << e.first << ") = " << e.second << endl;

    {
        cout << "--- #4 ---\n";
        map<unsigned int, unsigned int> values = populate(N);
        for (map<unsigned int, unsigned int>::iterator iter = values.begin();
            iter != values.end();
            ++iter)
        {
            pair<const unsigned, const unsigned> e = *iter;
            cout << "fib(" << e.first << ") = " << e.second << endl;
        }
    }

    return 0;
}

