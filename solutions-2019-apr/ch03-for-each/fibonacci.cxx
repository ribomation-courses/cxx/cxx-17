#include <iostream>
#include <iomanip>
#include <string>
using namespace std;
using namespace std::literals;

class Fibonacci {
    unsigned numValues;
public:
    Fibonacci(unsigned n) : numValues{n} {}
    ~Fibonacci() = default;
    Fibonacci(const Fibonacci&) = default;
    Fibonacci& operator =(const Fibonacci&) = default;

    struct iterator {
        unsigned long f2 = 0;
        unsigned long f1 = 1;
        unsigned      count;

        iterator(unsigned n) : count{n} {}
        bool          operator !=(iterator& /*rhs*/) {
//            return count != rhs.count;
            return count>0;
        }
        unsigned long operator *() {
            return f1;
        }
        void          operator ++() {
            auto f = f2 + f1;
            f2 = f1;
            f1 = f;
//            ++count;
            --count;
        }
    };

//    iterator begin() const { return {0}; }
//    iterator end()   const { return {numValues}; }
    iterator begin() const { return {numValues}; }
    iterator end()   const { return {0}; }
};

int main(int argc, char** argv) {
    auto      n = argc == 1 ? 92U : stoi(argv[1]);
    for (auto f : Fibonacci{n})
        cout << setw(20) << f << endl;
    return 0;
}
