#include <iostream>
#include <string>
#include <map>
#include <algorithm>
#include <cstring>
using namespace std;
using namespace std::literals;

auto lc(const string& s) {
    auto result = ""s;
    for_each(s.begin(), s.end(), [&result](auto ch) {
        if (::isalpha(ch)) result += ::tolower(ch);
    });
    return result;
}

int main() {
    auto      freqs = map<string, unsigned>{};
    for (auto word  = ""s; cin >> word;)
        if (auto w = lc(word); w.size() >= 4) ++freqs[w];

    for (auto const& [word, freq] : freqs)
        cout << word << ": " << freq << endl;

    return 0;
}
