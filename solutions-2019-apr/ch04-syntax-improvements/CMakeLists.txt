cmake_minimum_required(VERSION 3.10)
project(ch1_syntax_improvements)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -Wextra -Werror -Wfatal-errors)

add_executable(the-answer the-answer.cxx)
add_executable(no-copy no-copy.cxx)
add_executable(greet greet-func.hxx greet-func.cxx greet-app.cxx)
