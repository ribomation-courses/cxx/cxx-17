#include <iostream>
#include "greet-func.hxx"

using namespace std;
using namespace std::literals;

namespace {
    void greet() {
        cout << "Hello from app\n";
    }
}

int main() {
    greet();
    func();
    return 0;
}
