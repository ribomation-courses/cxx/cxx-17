#include <iostream>
#include "greet-func.hxx"

using namespace std;

namespace {
    void greet() {
        cout << "Hello from func\n";
    }
}

void func() {
    greet();
}

