#include <iostream>
using namespace std;

int main() {
    cout << "unsigned: " << 42U << endl;
    cout << "hex     : " << 0x2A << endl;
    cout << "octal   : " << 052 << endl;
    cout << "binary  : " << 0b101010 << endl;
    return 0;
}
