#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int        numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    const auto N         = sizeof(numbers) / sizeof(numbers[0]);

    const auto factor = 42U;
    transform(numbers, numbers + N, numbers, [=](auto x) {
        return x * factor;
    });

    for_each(numbers, numbers + N, [](auto x) {
        cout << x << " ";
    });
    cout << endl;
    return 0;
}
