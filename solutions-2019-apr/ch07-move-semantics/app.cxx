#include <iostream>
#include "movable-string.hxx"
using namespace std;
using namespace ribomation;

TinyString mk() {
    static const char* names[] = {"anna", "berit", "carin", "diana", "eva", "frida"};
    static auto N    = sizeof(names) / sizeof(char*);
    static int  next = 0;
    return {names[next++ % N]};
}

int main() {
    TinyString s1{"Tjabba Habba"};
    cout << "s1: " << s1 << endl;

    cout << "---- (1)\n";
    s1 = mk();
    cout << "s1: " << s1 << endl;

    cout << "---- (2)\n";
    {
        TinyString s2 = move(s1);
        cout << "s1: " << s1 << endl;
        cout << "s2: " << s2 << endl;

        cout << "---- (2b)\n";
        TinyString s3 = s2 + " -- " + mk();
        cout << "s3: " << s3 << endl;
    }

    cout << "---- (3)\n";
    {
        TinyString s4{mk()};
        cout << "s4: " << s4 << endl;

        TinyString s5 = mk();
        cout << "s5: " << s5 << endl;
    }

    //TinyString s4{s1};
    //error: use of deleted function ‘TinyString::TinyString(const TinyString&)’

    //TinyString s3 = "hepp";
    //s1 = s3;
    //error: use of deleted function ‘TinyString& TinyString::operator=(const TinyString&)’

    //TinyString s4;
    //error: use of deleted function ‘TinyString::TinyString()’

    cout << "---- (4)\n";
    return 0;
}
