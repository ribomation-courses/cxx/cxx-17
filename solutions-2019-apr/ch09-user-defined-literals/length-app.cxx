#include <iostream>
#include <iomanip>
#include "length.hxx"
using namespace std;
using namespace std::literals;
using namespace ribomation::length;

int main() {
    auto len = 2_km + 12.5_m - 0.5_mi + 31_ya;
    cout << "len: " << fixed << setprecision(3) << len << endl;
    return 0;
}
