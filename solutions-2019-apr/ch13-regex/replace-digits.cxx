#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>
#include <regex>

using namespace std;
using namespace std::literals;

void replace(istream& in) {
    auto      re     = regex{"\\d+"};
    auto      lineno = 1U;
    for (auto line   = ""s; getline(in, line); ++lineno) {
        auto result = regex_replace(line, re, "#");
        cout << lineno << ") " << result << endl;
    }
}

int main(int argc, char** argv) {
    auto filename = (argc > 1) ? argv[1] : "../numbers.txt"s;
    auto file     = ifstream{filename};
    if (!file) throw invalid_argument{"cannot open "s + filename};
    replace(file);
    return 0;
}

