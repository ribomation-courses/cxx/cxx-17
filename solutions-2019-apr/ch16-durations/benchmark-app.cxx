#include <iostream>
#include "elapsed-time.hxx"
#include "word-freqs.hxx"

using namespace std::literals;
using std::string;
using std::vector;
using std::cout;
using std::endl;
using ribomation::util::elapsed;

int main() {
    auto filename = "../shakespeare.txt"s;
    auto [ns, freqs] = elapsed<string, vector<WordFreq>>([](auto s) {
        return wordFreqs(s, 50);
    }, filename);

    for (auto[w, f] : freqs)cout << w << ": " << f << endl;
    cout << "----\nWord Count  : " << freqs.size() << endl;
    cout << "Elapsed Time: " << ns * 1E-9 << " seconds" << endl;

    return 0;
}
