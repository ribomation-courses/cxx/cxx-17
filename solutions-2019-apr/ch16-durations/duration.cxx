#include <iostream>
#include <chrono>
using namespace std;
using namespace std::chrono;
using namespace std::chrono_literals;

int main() {
    auto period = 3h + 7min + 49s;
    cout << "period is " << period.count() << " seconds"
         << ", which is " << duration_cast<microseconds>(period).count() << " us"
         << endl;
    return 0;
}
