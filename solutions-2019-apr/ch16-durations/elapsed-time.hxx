#pragma once
#include <tuple>
#include <functional>
#include <chrono>

namespace ribomation::util {
    using std::function;
    using std::tuple;
    using std::chrono::high_resolution_clock;
    using std::chrono::duration_cast;
    using std::chrono::nanoseconds;

    template<typename Argument, typename Return>
    auto elapsed(function<Return(Argument)> f, Argument arg) -> tuple<unsigned long, Return> {
        auto   start  = high_resolution_clock::now();
        Return result = f(arg);
        auto   end    = high_resolution_clock::now();
        return {duration_cast<nanoseconds>(end - start).count(), result};
    }

}

