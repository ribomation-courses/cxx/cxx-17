#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <string_view>
#include <stdexcept>
#include <vector>
#include <algorithm>
#include <tuple>
#include <utility>
#include <map>
#include <cstring>
#include <cerrno>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include "word-freqs.hxx"

using namespace std;
using namespace std::literals;

namespace {
    auto filesize(const string& filename) -> size_t {
        struct stat info{};
        if (stat(filename.data(), &info) == -1) {
            throw invalid_argument{"stat failed: "s + strerror(errno)};
        }
        return info.st_size;
    }
}

auto load(const string& filename) -> tuple<const char*, unsigned long> {
    auto size = filesize(filename);
    auto payload = new char[size + 1];
    auto fd = open(filename.c_str(), O_RDONLY);
    read(fd, payload, static_cast<size_t>(size + 1));
    close(fd);
    payload[size] = '\0';
    return {payload, size};
}

auto count(const char* payload, unsigned long size) -> map<string_view, unsigned>{
    auto freqs = map<string_view, unsigned>{};
    auto start = 0UL;
    do {
        while (!isalpha(payload[start]) && (start < size)) ++start;

        auto end = start;
        while (isalpha(payload[end]) && (start < size)) ++end;

        string_view word{&payload[start], end - start};
        if (word.size() > 3) ++freqs[word];

        start = end + 1;
    } while (start < size);
    return freqs;
}

auto wordFreqs(string filename, unsigned maxWords) -> vector<WordFreq> {
    auto[payload, size] = load(filename);
    auto freqs = count(payload, size);

    auto sorted = vector<WordFreq>{freqs.begin(), freqs.end()};
    sort(sorted.begin(), sorted.end(), [](WordFreq lhs, WordFreq rhs) {
        return lhs.second > rhs.second;
    });

    auto result = vector<WordFreq>{};
    copy_n(begin(sorted), maxWords, back_inserter(result));

    return result;
}

