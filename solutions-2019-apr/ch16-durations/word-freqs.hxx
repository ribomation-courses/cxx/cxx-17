#pragma once

#include <utility>
#include <string_view>
#include <vector>
#include <string>

using WordFreq = std::pair<std::string_view, unsigned>;

extern auto wordFreqs(std::string filename, unsigned maxWords = 100) -> std::vector<WordFreq>;

