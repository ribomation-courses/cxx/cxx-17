#include <random>
#include <string>
#include <algorithm>
#include "generator.hxx"
#include "account.hxx"

using namespace std;
using namespace std::literals;

namespace {
    random_device r;
}

string nextAccno(string pattern) {
    uniform_int_distribution<char> digit{'0', '9'};
    transform(pattern.begin(), pattern.end(), pattern.begin(), [&](auto ch) {
        return (ch == '#') ? digit(r) : ch;
    });
    return pattern;
}

float nextInterestRate() {
    uniform_real_distribution<float>    rates{0.5F, 5.5F};
    return rates(r);
}

int nextBalance() {
    normal_distribution<float>          amounts{1000, 400};
    return static_cast<int>(amounts(r));
}

Account nextAccount(string pattern) {
    return {nextAccno(pattern), nextInterestRate(), nextBalance()};
}

