#pragma once
#include "account.hxx"

extern Account nextAccount();
extern string nextAccno(string pattern = "SEB-###-#####"s);
extern float nextInterestRate();
extern int nextBalance();

