#pragma once

#include <mutex>
#include <condition_variable>
#include <queue>

namespace ribomation::threads {
    using namespace std;

    template<typename T>
    class MessageQueue {
        mutex              lck;
        condition_variable notEmpty;
        condition_variable notFull;
        queue<T>           inbox;
        const unsigned     capacity;

        bool empty() const { return inbox.size() == 0; }
        bool full()  const { return inbox.size() == capacity; }

    public:
        MessageQueue(unsigned max) : capacity{max} {}
        ~MessageQueue() = default;
        MessageQueue(const MessageQueue<T>&) = delete;
        MessageQueue<T>& operator =(const MessageQueue<T>&) = delete;

        void put(T x) {
            unique_lock<mutex> g{lck};
            notFull.wait(g, [this] { return !full(); });
            inbox.push(x);
            notEmpty.notify_all();
        }

        T get() {
            unique_lock<mutex> g{lck};
            notEmpty.wait(g, [this] { return !empty(); });
            T x = inbox.front();
            inbox.pop();
            notFull.notify_all();
            return x;
        }
    };
}
