#include <iostream>
#include <iomanip>
#include <locale>
#include <string>
#include <thread>
#include "message-queue.hxx"
using namespace std;
using namespace std::literals;
using namespace ribomation::threads;

struct Producer {
    const unsigned max;
    MessageQueue<unsigned>& out;
    Producer(unsigned max, MessageQueue<unsigned>& out) : max(max), out(out) {}
    void operator ()() {
        for (auto k = 1U; k <= max; ++k) out.put(k);
        out.put(0);
    }
};

struct Consumer {
    MessageQueue<unsigned>& in;
    Consumer(MessageQueue<unsigned>& in) : in(in) {}
    void operator ()() {
        for (auto msg = in.get(); msg != 0; msg = in.get())
            cout << "C: " << setw(6) << msg << endl;
    }
};

int main() {
    auto N = 100'000U;
    MessageQueue<unsigned> Q{16};
    thread prod{Producer{N, Q}};
    thread cons{Consumer{Q}};

    prod.join();
    cons.join();

    return 0;
}
