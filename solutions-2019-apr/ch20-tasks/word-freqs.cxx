#include <iostream>
#include <iomanip>
#include <fstream>

#include <string>
#include <string_view>
#include <stdexcept>

#include <memory>
#include <utility>
#include <tuple>
#include <algorithm>
#include <vector>
#include <unordered_map>
#include <chrono>
#include <future>

#include <cctype>
#include <cstring>
#include <cerrno>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

using namespace std;
using namespace std::literals;
using namespace std::chrono;

using Freqs = unordered_map<string_view, unsigned>;
using Freq = pair<string_view, unsigned>;

auto load(const string& filename) -> tuple<char*, unsigned long> {
    auto f = fstream{filename, ios::in | ios::binary};
    if (!f) throw invalid_argument{"cannot open "s + filename};

    f.seekg(0, ios::end);
    auto size = static_cast<unsigned long>(f.tellg());
    f.seekg(0, ios::beg);

    auto payload = new char[size];
    f.read(payload, size);

    return make_tuple(payload, size);
}

auto count(string_view payload) -> Freqs {
    auto freqs = Freqs{};

    auto start = 0UL;
    do {
        while (!isalpha(payload[start]) && (start < payload.length())) ++start;

        auto end = start;
        while (isalpha(payload[end]) && (start < payload.length())) ++end;

        auto word = payload.substr(start, end - start);
        if (word.size() > 3) ++freqs[word];

        start = end + 1;
    } while (start < payload.length());

    return freqs;
}

auto aggregate(vector<future<Freqs>>& partialResults) -> Freqs{
    auto results = Freqs{};
    for (auto& fut : partialResults) {
        for (auto&[word, freq] : fut.get()) results[word] += freq;
    }
    return results;
}

int main() {
    auto filename = "../shakespeare.txt"s;

    auto start = high_resolution_clock::now();
    auto[content, size] = load(filename);
    transform(content, content + size, content, [](auto ch) {
        return ::tolower(ch);
    });

    auto numTask        = thread::hardware_concurrency();
    auto chunkSize      = size / numTask;
    auto payload        = string_view{content, size};
    auto partialResults = vector<future<Freqs>>{};

    for (auto k = 0U; k < numTask; ++k) {
        auto chunk = payload.substr(k * chunkSize, chunkSize);
        partialResults.push_back(async(launch::async, [=]() { return count(chunk); }));
    }
    auto      results  = aggregate(partialResults);
    auto      sortable = vector<Freq>{results.begin(), results.end()};
    sort(sortable.begin(), sortable.end(), [](Freq lhs, Freq rhs) {
        return lhs.second > rhs.second;
    });
    auto end = high_resolution_clock::now();
    auto elapsed = duration_cast<nanoseconds>(end - start);

    for (auto&[word, freq] : sortable)
        cout << word << ": " << freq << "\n";

    cout.imbue(locale{"en_US.UTF8"s});
    cout << "------\n";
    cout << "# Tasks     : " << numTask << "\n";
    cout << "File Size   : " << size << " bytes\n";
    cout << "Chunk Size  : " << chunkSize << " bytes\n";
    cout << "Elapsed Time: " << elapsed.count() * 1E-9 << " seconds\n";
    delete [] content;

    return 0;
}


