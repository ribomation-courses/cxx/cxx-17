#include <iostream>
#include <initializer_list>
#include <numeric>
#include <algorithm>

using namespace std;
using namespace std::literals;

class Stats {
    unsigned long cnt;
    unsigned long sum;
    unsigned int minVal;
    unsigned int maxVal;

public:
    Stats(initializer_list<int> ints) {
        cnt = ints.size();
        sum = accumulate(ints.begin(), ints.end(), 0);
        minVal = *min_element(ints.begin(), ints.end());
        maxVal = *max_element(ints.begin(), ints.end());
    }

    long count() {
        return cnt;
    }
    double average() {
        return static_cast<double>(sum) / cnt;
    }
    int min() {
        return minVal;
    }
    int max() {
        return maxVal;
    }
};

int main(int argc, char** argv) {
    Stats s = {1,2,3,4,5,6,7,8,9,10};
    cout << "# values: " << s.count() << endl;
    cout << "average : " << s.average() << endl;
    cout << "min     : " << s.min() << endl;
    cout << "max     : " << s.max() << endl;
    return 0;
}
