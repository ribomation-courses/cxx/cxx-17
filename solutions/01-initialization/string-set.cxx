#include <iostream>
#include <string>
#include <set>
using namespace std;
using namespace std::literals;

int main() {
    set<string> words = {
            "hi", "ho", "hello", "howdy"
    };
    for (string w : words) cout << w << " ";
    cout << endl;
    return 0;
}
