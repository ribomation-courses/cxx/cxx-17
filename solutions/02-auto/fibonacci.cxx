#include <iostream>
#include <tuple>
#include <map>
#include <string>
using namespace std;

auto fib(unsigned n) {
    if (n == 0) return 0U;
    if (n == 1) return 1U;
    return fib(n - 2) + fib(n - 1);
}

auto compute(unsigned n) {
    return make_tuple(n, fib(n));
}

auto populate(unsigned n) {
    auto      values = map<unsigned, unsigned>{};
    for (auto k = 1U; k <= n; ++k) {
        auto[arg, result] = compute(k);
        values[arg] = result;
    }
    return values;
}

int main(int argc, char** argv) {
    auto n      = (argc==1) ? 10U : stoi(argv[1]);
    for (auto[arg, result] : populate(n)) {
        cout << "fib(" << arg << ") = " << result << endl;
    }
    return 0;
}


