#include <iostream>
#include <string>

using namespace std;
using namespace std::literals;

class Fibonacci {
    const unsigned N;
public:
    Fibonacci(unsigned n) : N{n} {}

    struct iterator {
        unsigned long long f0 = 0;
        unsigned long long f1 = 1;
        unsigned N;

        iterator(unsigned n) : N{n} {}

        auto operator *() const {
            return f1;
        }
        void operator ++() {
            auto fib = f0 + f1;
            f0 = f1;
            f1 = fib;
            ++N;
        }


        bool operator !=(const iterator& rhs) const {
            return this->N != rhs.N;
        }
    };

    iterator begin() const {
        return {1};
    }

    iterator end() const {
        return {N + 1};
    }
};

int main(int argc, char** argv) {
    auto const N = (argc == 1) ? 10U : stoi(argv[1]);
    for (auto  f : Fibonacci{N}) cout << f << endl;
    return 0;
}
