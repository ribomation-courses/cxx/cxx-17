#include <iostream>
#include <string>
#include <map>
#include <cctype>
using namespace std;

auto strip(const string& s) {
    auto      r = ""s;
    for (auto ch : s) {
        if (::isalpha(ch)) r += ::tolower(ch);
    }
    return r;
}

int main() {
    auto freqs = map<string, unsigned>{};
    for (string word; cin >> word;) {
        word = strip(word);
        if (word.size() > 1) ++freqs[word];
        //freqs[word]
        //   --> freqs.operator[](word)
        //       --> REF( make_pair(word, 0U).second )
    }
//    for (auto const& p : freqs)
//        cout << p.first << ": " << p.second << endl;
    for (auto const& [word, freq] : freqs)
        cout << word << ": " << freq << endl;

    return 0;
}
