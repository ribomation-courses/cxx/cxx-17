#include <iostream>

using namespace std;

struct NoCopy {
    int value;

    explicit NoCopy(int value) : value(value) {}

    NoCopy(const NoCopy&) = delete;
    auto operator =(const NoCopy&) -> NoCopy& = delete;

    friend auto operator <<(ostream& os, const NoCopy& obj) -> ostream& {
        return os << "NoCopy(" << obj.value << ") @ " << &obj;
    }
};

void func(NoCopy obj) {}

int main() {
    NoCopy obj{42};
    cout << "obj: " << obj << endl;

    //NoCopy obj2{obj};
    //error: use of deleted function NoCopy::NoCopy(const NoCopy&)

    //func(obj);
    //error: use of deleted function NoCopy::NoCopy(const NoCopy&)

    return 0;
}


