#include <iostream>
using namespace std;

int main() {
    cout << "Unsigned: " << 42U << endl;
    cout << "Hex     : " << 0x2A << endl;
    cout << "Octal   : " << 052 << endl;
    cout << "Binary  : " << 0b00101010 << endl;
return 0;
}
