#include <iostream>
#include <stdexcept>
#include <string>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

auto func(Person q) -> Person {
    cout << "[func] q: " << q << endl;
    q.incrAge();
    cout << "[func] q: " << q << endl;
    return q;
}

Person g{"Anna Conda", 42};

int main() {
    cout << "[main] g: " << g << endl;

    cout << "-------\n";
    auto p = Person{"Cris P. Bacon", 27};
    cout << "[main] p: " << p << endl;

    cout << "-------\n";
    auto q = func(p);
    cout << "[main] q: " << q << endl;

    cout << "-------\n";
    q = func(move(p));
    cout << "[main] q: " << q << endl;

    cout << "-------\n";
    return 0;
}
