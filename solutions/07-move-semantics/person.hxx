#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <cstring>

namespace ribomation {
    using std::ostream;
    using std::ostringstream;
    using std::cout;
    using std::endl;

    class Person {
        char* name = nullptr;
        unsigned age = 0;

        static char* copystr(const char* str) {
            if (str == nullptr) return nullptr;
            return strcpy(new char[strlen(str) + 1], str);
        }

    public:
        Person() {
            cout << "Person() @ " << this << endl;
            name = copystr("");
        }

        ~Person() {
            cout << "~Person() @ " << this << endl;
            delete[] name;
        }

        Person(const char* n, unsigned a)
                : name{copystr(n)}, age{a} {
            cout << "Person("<<name<<") @ " << this << endl;
        }

        Person(const Person& that)
                : name{copystr(that.name)}, age{that.age} {
            cout << "Person(const Person& "<<name<<") @ " << this << endl;
        }

        Person(Person&& that) noexcept
                : name{std::move(that.name)}, age{that.age} {
            that.name = nullptr;
            that.age  = 0;
            cout << "Person(Person&& "<<name<<") @ " << this << endl;
        }

        auto operator =(const Person& that) -> Person& {
            if (this != &that) {
                delete[] name;
                name = copystr(that.name);
                age  = that.age;
            }
            return *this;
        }

        auto operator =(Person&& that) noexcept -> Person& {
            if (this != &that) {
                delete[] name;
                name = that.name;
                that.name = nullptr;
                age = that.age;
                that.age = 0;
            }
            return *this;
        }

        unsigned incrAge() {
            return ++age;
        }

        std::string toString() const {
            ostringstream buf{};
            buf << "Person(" << name << ", " << age << ") @ " << this;
            return buf.str();
        }

        friend auto operator <<(ostream& os, const Person& p) -> ostream& {
            return os << p.toString();
        }

    };

}
