#include <iostream>
#include "math-lib.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

void bizop(double x) {
    cout << "log[2]("<<x<<") = " << log_2(x) << endl;
}

void func2(double x) {
    bizop(x);
}

void func1(double x) {
    func2(x);
}

void invoke(double x) {
    try {
        func1(x);
    } catch (const MathError& x) {
        cout << "ERROR: " << x.what() << endl;
        x.stackTrace.printStackTrace(cout);
    }
}

int main() {
//    invoke(1024);
    invoke(0);
//    invoke(-17);
    return 0;
}
