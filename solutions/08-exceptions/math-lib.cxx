#include <sstream>
#include <iostream>
#include <string>
#include <regex>

#include <cmath>
#include <cstring>
#include <cerrno>

#include <execinfo.h>
#include <cxxabi.h>

#include "math-lib.hxx"

// How to generate a stack-trace in GCC
// https://stackoverflow.com/questions/77005/how-to-automatically-generate-a-stacktrace-when-my-program-crashes
// http://www.gnu.org/software/libc/manual/html_node/Backtraces.html
// https://gcc.gnu.org/onlinedocs/libstdc++/manual/ext_demangling.html

namespace ribomation {
    using namespace std::string_literals;
    using std::ostringstream;
    using std::string;

    // /mnt/c/Users/..../cmake-build-debug/math-error(_ZN10ribomation5log_2Ed+0xbf) [0x402911]
    auto parseFunctionName(const string& stkFrameInfo) -> string {
        //std::cout << "parseFunctionName: " << stkFrameInfo << std::endl;
        auto pattern = std::regex{".+\\((.+)\\+.+\\).+"s};
        auto match   = std::smatch{};
        if (std::regex_match(stkFrameInfo, match, pattern)) {
            auto linkerName = match[1].str();
            //std::cout << "parseFunctionName: " << "linkerName: '" << linkerName << "'\n";

            string cxxName;
            if (linkerName == "main"s) {
                cxxName = "main"s;
            } else {
                int  status;
                auto cxxNamePtr = abi::__cxa_demangle(linkerName.c_str(), 0, 0, &status);
                cxxName = cxxNamePtr == nullptr ? "libstdc++"s : string{cxxNamePtr};
                free(cxxNamePtr);
            }
            //std::cout << "parseFunctionName: " << "cxxName: '" << cxxName << "'\n";

            return cxxName;
        }
        return "???";
    }

    auto StackTrace::create(unsigned offset) -> vector<string> {
        auto stack = vector<string>{};

        const auto MAX_STACK = 1024U;
        void* stackTraceAddresses[MAX_STACK];
        size_t numFrames = backtrace(stackTraceAddresses, MAX_STACK);
        char** stackTrace = backtrace_symbols(stackTraceAddresses, numFrames);
        for (auto k = offset; k < numFrames; ++k) {
            auto stkInfo  = stackTrace[k];
            auto funcName = parseFunctionName(stkInfo);
            stack.push_back(funcName);
        }
        free(stackTrace);

        return stack;
    }

    void StackTrace::printStackTrace(std::ostream&) const {
        auto k          = 0U;
        for (auto const& func: stack)
            std::cout << k++ << ": " << func << std::endl;
    }


    auto log_2(double x) -> double {
        auto name = parseFunctionName(
                "/mnt/c/..../cmake-build-debug/math-error(_ZN10ribomation5log_2Ed+0xbf) [0x402911]"s);
        std::cerr << "func: " << name << std::endl;

        string const   filename = __FILE__;
        unsigned const lineno   = __LINE__;
        auto           result   = std::log2(x);
        if (std::isfinite(result)) {
            return result;
        }

        if (std::isnan(result)) {
            throw MathError{filename, lineno, "log"s, x, "NaN"s};
        }

        if (std::isinf(result)) {
            throw MathError{filename, lineno, "log"s, x, "oo"s};
        }

        throw MathError{filename, lineno, "log"s, x, strerror(errno)};
    }

    string MathError::fmt(const string& file, unsigned line, const string& func, double arg, const string& what) {
        auto buf = ostringstream{};
        buf << file << ":" << line << " - " << func << "(" << arg << ") --> " << what;
        return buf.str();
    }




}
