#pragma once

#include <stdexcept>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

namespace ribomation {
    using std::runtime_error;
    using std::string;
    using std::ostringstream;
    using std::vector;

    struct StackTrace {
        vector<string> stack;

        StackTrace(vector<string> stk) : stack{std::move(stk)} {}

        void printStackTrace(std::ostream& os = std::cout) const;
        static auto create(unsigned offset = 1) -> vector<string>;
    };

    struct MathError : runtime_error {
        StackTrace stackTrace;

        explicit MathError(const string& file, unsigned line,
                           const string& func, double arg,
                           const string& what)
                : runtime_error{fmt(file, line, func, arg, what)},
                  stackTrace{StackTrace::create(2)} {}

        static string fmt(const string& file, unsigned line,
                          const string& func, double arg,
                          const string& what);
    };

    auto log_2(double x) -> double;
}
