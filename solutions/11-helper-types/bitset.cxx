#include <iostream>
#include <string>
#include <bitset>
using namespace std;
using namespace std::literals;

int main(int argc, char** argv) {
    auto a = argc <= 1 ? 3U : stoi(argv[1]);
    auto b = argc <= 2 ? 42U : stoi(argv[2]);
    cout << "a=" << a << ", b=" << b << endl;

    bitset<8> A{a}, B{b};
    cout << "A=" << A << "\nB=" << B << endl;

    cout << "A & B  = " << (A & B)  << endl;
    cout << "A | B  = " << (A | B)  << endl;
    cout << "A ^ B  = " << (A ^ B)  << endl;
    cout << "~A     = " << (~A)     << endl;
    cout << "A << 2 = " << (A << 2) << endl;
    cout << "B >> 3 = " << (B >> 3) << endl;
    return 0;
}
