#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <string_view>
#include <stdexcept>
#include <vector>
#include <algorithm>
#include <tuple>
#include <utility>
#include <unordered_map>
#include <chrono>
#include <cstring>
#include <cerrno>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

using namespace std;
using namespace std::literals;
using namespace std::chrono;
using WordFreqs = unordered_map<string_view, unsigned>;
using WordFreq  = pair<string_view, unsigned>;


auto load(const string& filename) -> tuple<const char*, unsigned long> {
    struct stat info{};
    if (stat(filename.data(), &info) == -1) {
        throw invalid_argument{"stat failed: "s + strerror(errno)};
    }
    auto size = info.st_size;

    char* payload = new char[size + 1];
    auto fd = open(filename.c_str(), O_RDONLY);
    read(fd, payload, static_cast<size_t>(size + 1));
    close(fd);
    payload[size] = '\0';

    return {payload, size};
}

auto count(const char* payload, unsigned long size, unsigned N) -> WordFreqs {
    auto freqs = WordFreqs{};
    auto start = 0UL;
    do {
        while (!isalpha(payload[start]) && (start < size)) ++start;

        auto end = start;
        while (isalpha(payload[end]) && (start < size)) ++end;

        string_view word{&payload[start], end - start};
        if (word.size() >= N) ++freqs[word];

        start = end + 1;
    } while (start < size);

    return freqs;
}

auto sort(const WordFreqs& freqs) -> vector<WordFreq> {
    auto sortable = vector<WordFreq>{freqs.begin(), freqs.end()};
    sort(sortable.begin(), sortable.end(), [](WordFreq lhs, WordFreq rhs) {
        return lhs.second > rhs.second;
    });
    return sortable;
}

void print(const vector<WordFreq>& freqs, unsigned N) {
    unsigned cnt = 1;
    for (auto [word, freq] : freqs) {
        cout << setw(12) << word << ": " << freq << endl;
        if (++cnt > N) break;
    }
}

int main() {
    auto filename    = "../musketeers.txt"s;
    auto minWordSize = 4U;
    auto maxWords    = 100U;

    auto startTime = high_resolution_clock::now();
    auto[payload, size] = load(filename);
    cout << "file.size  =" << setw(8) << size << " chars"
         << " (" << setprecision(3) << (size / (1024 * 1024.)) << " MB)"
         << endl;

    auto freqs = count(payload, size, minWordSize);
    cout << "freqs.size =" << setw(8) << freqs.size() << " words" << endl;

    auto sortedFreqs = sort(freqs);

    cout << "the " << maxWords << " most frequent words in " << filename << endl;
    print(sortedFreqs, maxWords);

    auto endTime      = high_resolution_clock::now();
    auto elapsedTime = duration_cast<milliseconds>(endTime - startTime).count();
    cout << "elapsed " << elapsedTime << " milliseconds" << endl;

    return 0;
}

