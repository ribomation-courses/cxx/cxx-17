#pragma once

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>

namespace ribomation::io {
    using namespace std;
    using namespace std::literals;
    namespace fs = std::filesystem;

    struct Count {
        const string name;
        unsigned     lines = 0;
        unsigned     words = 0;
        unsigned     chars = 0;

        Count(const string& name) : name{name} {}

        Count(const fs::path& file) : name{file.string()} {
            update(file);
        }

        ~Count() = default;
        Count(const Count&) = default;
        Count(Count&&) = default;

        void update(const fs::path& file) {
            chars += fs::file_size(file);

            ifstream    f{file};
            for (string line; getline(f, line);) {
                ++lines;
                istringstream buf{line};
                for (string   word; buf >> word;) ++words;
            }
        }

        void update(const Count& cnt) {
            lines += cnt.lines;
            words += cnt.words;
            chars += cnt.chars;
        }

        friend ostream& operator <<(ostream& os, const Count& cnt) {
            return os << setw(6) << cnt.lines << setw(8) << cnt.words << setw(10) << cnt.chars << "\t" << cnt.name;
        }
    };
}
