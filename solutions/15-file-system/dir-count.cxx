#include <iostream>
#include <filesystem>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include "count.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::io;
namespace fs = std::filesystem;
using FileStats = unordered_map<string, Count>;

bool isTextFile(const fs::path& p) {
    static const unordered_set<string> TEXT_EXT = {".txt", ".cxx", ".hxx"};
    return TEXT_EXT.count(p.extension().string()) > 0;
}

int main(int argc, char** argv) {
    auto dir = fs::path{argc == 1 ? "../.." : argv[1]};
    if (!fs::is_directory(dir)) { dir = fs::current_path(); }

    cout << "Dir: " << fs::canonical(dir) << endl;
    Count total("TOTAL"s);
    for (const auto& e : fs::recursive_directory_iterator{dir}) {
        auto p = e.path();
        if (fs::is_regular_file(p) && isTextFile(p)) {
            Count cnt{p};
            cout << cnt << endl;
            total.update(cnt);
        }
    }
    cout << total << endl;
    return 0;
}
