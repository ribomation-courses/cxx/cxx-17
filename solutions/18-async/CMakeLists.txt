cmake_minimum_required(VERSION 3.10)
project(ch20_tasks LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_executable(word-freqs word-freqs.cxx)
target_compile_options(word-freqs PRIVATE -Wall -Wextra -Werror -Wfatal-errors)
target_link_libraries(word-freqs -pthread)
