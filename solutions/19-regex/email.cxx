#include <iostream>
#include <iomanip>
#include <string>
#include <regex>
#include <algorithm>

using namespace std;
using namespace std::literals;

bool isEmail(const string& txt) {
    regex pattern{R"([a-z]+\.[a-z]+@[a-z]+\.[a-z]+)"};
    return regex_match(txt, pattern);
}

int main(int argc, char** argv) {
    for_each(argv + 1, argv + argc, [](string arg) {
        cout << arg << " -> " << boolalpha << isEmail(arg) << endl;
    });
    return 0;
}

