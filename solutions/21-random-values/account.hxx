#pragma once

#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include "instance-count.hxx"

using namespace std;

class Account : public instance {
    const string accno;
    const float  rate;
    int          balance;

public:
    Account(const string& accno, float rate, int balance)
            : accno{accno}, rate{rate}, balance{balance} {
        cout << "Account{" << accno << ", SEK " << balance << ", " << rate * 100 << "%} @ " << this << endl;
    }

    ~Account() override {
        cout << "~Account() @ " << this << " (#objs=" << instance::count() << ")" << endl;
    }


    Account() = delete;
    Account(const Account&)= delete;
    Account& operator =(const Account&) = delete;
    Account(Account&&)= default;

    Account& operator +=(int amount) {
        balance += amount;
        return *this;
    }

    string toString() const {
        ostringstream buf;
        buf << "Account{" << accno << ", SEK " << balance << ", "
            << setprecision(2) << rate << "%} [#objs=" << instance::count() << "]";
        return buf.str();
    }

    friend ostream& operator <<(ostream& os, const Account& a) {
        return os << a.toString();
    }
};

