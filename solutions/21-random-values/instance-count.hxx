#pragma  once

class instance {
    static int _count;
protected:
    instance() { _count++; }
public:
    virtual ~instance() { _count--; }
    instance(const instance&) : instance{} {  }
    static int count() { return _count; }
};
