#include <iostream>
#include <vector>
#include "generator.hxx"

using namespace std;
using namespace std::literals;

int main() {
    auto N = 10U;
    {
        vector<Account> accounts;
        accounts.reserve(N);

        cout << "--- Creating " << N << " accounts\n";
        for (auto k = 0U; k < N; ++k)
            accounts.emplace_back(nextAccno(), nextInterestRate(), nextBalance());

        cout << "--- Printing accounts\n";
        for (auto& a : accounts) cout << a << endl;

        cout << "--- Destroying accounts\n";
    }
    cout << "# Accounts = " << Account::count() << endl;
    return 0;
}
